import phaser from 'phaser'
import Player from '../actors/player'
import Hexagon from '../actors/hexagon'
import UI from '../actors/ui'
import HexagonContainer from '../actors/hexagon-container'
import Map from '../actors/map'
import { drawWireHexagons } from '../lib/grid'

export class Preloader extends phaser.Scene {

    constructor () {
        
        super({
            key: 'preloader',
            physics: {
                default: 'arcade',
                arcade: {
                    debug: true,
                    gravity: { y: 200 }
                }
            }
        })
        window.game.scene = this;
        this.actors = [];
    }
    preload () {
        this.load.setBaseURL('http://127.0.0.1:5000/');
        this.load.image('hexagon', 'assets/hexaCanMerge.png');
        this.load.image('hexaYes', 'assets/hexaYes.png');
        this.load.image('window', 'assets/Window.png');
        this.load.image('crystalSword', 'assets/crystalSword.png');
    }

    create () {
        // this.actors.sky = this.add.image(300, 300, 'hexagon'); 

        // var particles = this.add.particles('hexagon');

        // var emitter = particles.createEmitter({
        //     speed: 100,
        //     scale: { start: 1, end: 0 },
        //     blendMode: 'ADD' 
        // });
        // emitter.setScale(1.2);
        // emitter.setPosition(window.innerWidth/2, window.innerHeight/2);

        // var logo = this.physics.add.image(100, 100, 'hexagon');
        // logo.setScale(.5);
        // logo.setVelocity(100, 200);
        // logo.setBounce(1, 1);
        // logo.setCollideWorldBounds(true);

        // emitter.startFollow(logo);


        // this.player = new Player({
        //     scene: this,
        //     key: 'hexagon',
        //     x: 300,
        //     y: 200
        // });


        // this.hexagonContainer = new HexagonContainer({ scene: this });
        this.map = new Map({ scene: this });
        // console.log(drawWireHexagons);
        drawWireHexagons(this, 200, 100);
        drawWireHexagons(this, 280, 145);
        // this.ui = new UI({ scene: this, x: window.innerWidth / 2, y: 0 });
        // this.children.sortGameObjects([this.hexagonContainer, this.ui]);
        // this.hexagonContainer.depth = 200;

        // this.setInteractive();
        // this.setDraggable(this);
        
        this.input.on('drag', (pointer, gameObject) => {
            gameObject.drag(pointer);
        });
        this.input.on('dragstart', (pointer, gameObject) => {
            if (gameObject.dragStart) gameObject.dragStart(pointer, gameObject);
        });

        this.input.on('dragend', (pointer, gameObject) => {
            gameObject.dragStop(pointer);
        });
        
        // this.pressX = this.add.bitmapText(16 * 8 + 4, 8 * 16, 'font', 'PRESS X TO START', 8);
    }

    update(time, delta) {
        super.update();
        // console.log(this);
        // this.actors.sky.y = this.actors.sky.y - 0.1; 
        // this.actors.sky.x = this.actors.sky.x - 0.1; 
        // this.actors.container.x = this.actors.container.x - 0.1;
        // this.actors.container.scale = this.actors.container.scale + 0.01;
        // console.log("updated");
        // this.player.update(time, delta);
        // this.ui.update(time, delta, this.game.loop.actualFps);
        // this.hexagonContainer.update(time, delta);
        // this.map.update(time, delta);
    }


    windowUpdate() {
        // this.hexagonContainer.updatePosition();
        // this.map.updatePosition();
    }
}