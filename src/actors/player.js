import Actor from '../lib/actor';
import Phaser from 'phaser';


export default class Player extends Phaser.GameObjects.Sprite {

    constructor(config) {
        super(config.scene, config.x, config.y - 16, config.key);
        this.alive = true;
        config.scene.add.existing(this);
        this.on('pointerdown', this.clickMe, this);
        this.setInteractive();

    }

    clickMe() {
        this.alpha -= .1;
    }

    update(time, delta) {
        this.x += 0.1;
    }

}