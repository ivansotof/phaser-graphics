import Phaser from 'phaser';


export default class Hexagon extends Phaser.GameObjects.Sprite {

    constructor(config) {
        super(config.scene, config.x, config.y, "hexagon");
        this.alive = true;
        config.scene.add.existing(this);
        
        this.inputEnabled = true;
        
        
        
        
        // console.log(config.scene);
        // this.inputEnabled = true;
        
        this.initialX = this.x;
        this.initialY = this.y;

        // this.image = config.key;
        this.children = {};
        this.children.icon = config.scene.add.image(0,0, "crystalSword");
        this.children.icon.x = this.x;
        this.children.icon.y = this.y;
        this.children.icon.setScale(0.4);

        this.setScale(.2);
        this.setInteractive();


        this.on('pointerover', function(pointer, localX, localY, event){
            this.pointerOver();
        });
        this.on('pointerout', function(pointer, localX, localY, event){
            this.pointerOut();
        });

        config.scene.input.setDraggable(this);
        // this.add(config.scene.add.image(0,0, 'window'));
        

    
        // this.input.on('drag', function (pointer, gameObject, dragX, dragY) {
        //     gameObject.x = dragX;
        //     gameObject.y = dragY;
        // });
    
        // this.input.on('dragend', function (pointer, gameObject) {
        //     gameObject.clearTint();
        //     gameObject.disableInteractive();
        // });
        // config.scene.physics.world.enable(this);
        // this.body.setGravityY(0);

    }

    pointerOver() {
        this.setTexture('hexaYes');
    }

    pointerOut() {
        this.setTexture('hexagon');
    }
    // clickMe() {
    //     // this.setScale(this.scale - 0.1);
    //     // console.log('lala');
    // }

    dragStart(pointer, gameObject) {
        gameObject.depth = 200;
    }

    drag(pointer) {
        // this.setPosition(pointer.x, pointer.y);
        // this.x = this.x;
        // console.log(`${this.parentContainer.x}`);
        // console.log(`${this.parentContainer.x - pointer.x}`);
        // console.log(`${this.x} + ${pointer.x}`);
        // console.log(this.parentContainer);
        this.x = (this.parentContainer.x - pointer.x ) *  (-1);   // the distance is proportional to the scale.
        this.y = (this.parentContainer.y - pointer.y ) *  (-1);   // the distance is proportional to the scale.
        
        //moving children
        for (let [key, c] of Object.entries(this.children)) {
            c.x = (this.parentContainer.x - pointer.x ) *  (-1); 
            c.y = (this.parentContainer.y - pointer.y ) *  (-1);  
        }
    }

    dragStop(pointer) {
        this.setPosition(this.initialX, this.initialY);
        this.depth = 1;
        for (let [key, c] of Object.entries(this.children)) {
            c.x = this.initialX; 
            c.y = this.initialY;  
        }
    }


}