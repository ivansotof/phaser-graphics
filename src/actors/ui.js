import Phaser from 'phaser';

export default class UI extends Phaser.GameObjects.Container {

    constructor(config) {
        super(config.scene, config.x, config.y - 16);
        this.alive = true;
        config.scene.add.existing(this);

        this.setInteractive(new Phaser.Geom.Circle(0, 0, 100), Phaser.Geom.Circle.Contains);
        
        this.on('pointerdown', this.clickMe, this);
        this.bg = config.scene.add.image(0,0, 'window');
        // this.bg.setScale(3, 3);
        this.add(this.bg);
        this.bg.setOrigin(0);
        // this.setInteractive();
        this.setX(0);
        this.setY(0);
        
        config.scene.input.setDraggable(this);
        this.depth = 100;

        this.text = config.scene.add.text(10, 10, "debug here", { fontFamily: 'Verdana, "Times New Roman", Tahoma, serif', color: "000" });
        this.text.depth = 101;
        
    }

    clickMe() {
        // this.x+=0.6;
    }

    update(time, delta, fps) {
        // console.log(this.x);
        // this.x -= 0.1;
        // this.x+=0.6;
        this.text.setText(fps);
    }

    drag(pointer) {
        // this.setPosition(pointer.x, pointer.y);
        // this.x = this.x;
        // console.log(`${this.parentContainer.x}`);
        // console.log(`${this.parentContainer.x - pointer.x}`);
        // console.log(`${this.x} + ${pointer.x}`);
        // console.log(this.parentContainer);
        this.x = pointer.x;   // the distance is proportional to the scale.
        this.y = pointer.y;   // the distance is proportional to the scale.
        // console.log("drag!");
        // this.depth = 200;
    }

    dragStop(pointer) {
        // this.setPosition(this.initialX, this.initialY);
        // this.depth = 1;
    }

}