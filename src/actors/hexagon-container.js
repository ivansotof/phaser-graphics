import Phaser from 'phaser';
import Hexagon from './hexagon';

export default class HexagonContainer extends Phaser.GameObjects.Container {

    constructor(config) {
        super(config.scene, window.innerWidth /2, window.innerHeight /2); //
        config.scene.add.existing(this);

        const horizontalHalf = 50; // width will be 2x
        const verticalHalf = 88;
        const radius = 3;

        const offsetX = 0;
        const offsetY = 0;

        for (let i = -radius; i <= radius; i++) {
            const r1 = Math.max(-radius, -i - radius);
            const r2 = Math.min(radius, -i + radius);

            for (let j = r1; j <= r2; j++) {
                let hexagon = new Hexagon({
                    scene: config.scene,
                    key: 'hexagon',
                    x: offsetX + (horizontalHalf * j + (horizontalHalf*2*i)),
                    y: offsetY + (-j * verticalHalf)
                });
                this.add(hexagon);
                // console.log(hexagon.children);
                for (let [key, c] of Object.entries(hexagon.children)) {
                    // console.log(key, c);
                    this.add(c);
                }
            }
        }
        window.group = this;
        // this.setScale(1);
    }


    update(time, delta) {
        // console.log(this.x);
        // this.x -= 0.1;
        // this.x+=0.6;
    }

    updatePosition() {
        this.setPosition(window.innerWidth /2, window.innerHeight /2);
    }

}