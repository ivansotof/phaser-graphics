
import Phaser from 'phaser';

const Hexagon = { w: 50, h: 20 };
const Line = { w: 1 };
let lines = [];

function drawWireHexagons(scene, x = 200, y = 100) {
    
    window.game = window.game || {};
    window.game.radius = window.game.radius || 30;
    let shape = [];

    //Get the middle of the panel
    let x_0 = x;
    let y_0 = y;
    let r = window.game.radius;

    // clear
    lines.forEach(line => {
        line.destroy();
    });
    lines = [];
    // console.log(lines.length)
    
    // grid radius, not hexagon
    let radius = 5,
        offsetX = window.innerWidth / 2,
        offsetY = 300;

    for (let i = -radius; i <= radius; i++) {
        const r1 = Math.max(-radius, -i - radius);
        const r2 = Math.min(radius, -i + radius);

        for (let j = r1; j <= r2; j++) {
            // let hexagon = new Hexagon({
            //     scene: config.scene,
            //     key: 'hexagon',
            //     x: offsetX + (horizontalHalf * j + (horizontalHalf*2*i)),
            //     y: offsetY + (-j * verticalHalf)
            // });
            const x = offsetX + (50 * j + (50*2*i));
            const y =  offsetY + (-j * 40);

            //Create 6 points
            for(let a = 0; a <= 6; a++)
            {
                shape[a] = [
                    x + r * Math.sin(a * 60 * Math.PI / 180), 
                    y + r * Math.cos(a * 60 * Math.PI / 180) //*0.6
                ]
                
                if (a == 0) {
                    scene.add.line(0, 0, shape[a][0], shape[a][1], shape[0][0], shape[0][1], 0x990000, 1).setOrigin(0, 0)
                } else {
                    let myLine = new Phaser.GameObjects.Line(scene, 0, 0, shape[a][0], shape[a][1], shape[a-1][0], shape[a-1][1], 0x990000, 1);
                    lines.push(myLine);
                    myLine.setOrigin(0,0)
                    scene.add.existing(myLine);
                    // scene.add.line(0, 0, shape[a][0], shape[a][1], shape[a-1][0], shape[a-1][1], 0x990000, 1).setOrigin(0, 0)
                }
                    // var line = scene.add.line();
            }
            
        }
    }




    
}

window.drawWireHexagons = drawWireHexagons;

export { 
    drawWireHexagons 
}