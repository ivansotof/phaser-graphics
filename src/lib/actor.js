import Phaser from 'phaser';


export default class Actor extends Phaser.GameObjects.Sprite {

    constructor(config) {
        super(config.scene, config.x, config.y - 16, config.key);
        this.alive = true;
    }

}