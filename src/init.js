import Loop from './loop';
import Phaser from 'phaser'
import { Preloader } from './scenes/preloader'


import * as THREE from 'three'
import { PerspectiveCamera, Scene, Vector3 } from 'three';
import { TrackballControls } from './TrackballControls'
import GLTFLoader from 'three-gltf-loader';

window.game = {
    radius: -1.5708
}

const mouse = new THREE.Vector2()
let ray;
let INTERSECTED;

const renderer = new THREE.WebGLRenderer()
let container = document.getElementById('play')
container.appendChild(renderer.domElement)

// camera
const camera = new PerspectiveCamera(70, window.innerWidth / window.innerHeight, 1, 10000)
camera.position.set(0, 110, 155)
camera.lookAt( 0, 0, 0)


let scene = new Scene()
window.scene = scene

const baseGeo = new THREE.BoxGeometry(170, 1 , 90)
const baseMaterial = new THREE.MeshPhongMaterial( {color: 0xaa703f})
const baseMesh = new THREE.Mesh(baseGeo, baseMaterial)
baseMesh.receiveShadow = true;
baseMesh.castShadow = true
scene.add(baseMesh)

// floor
var geoFloor = new THREE.PlaneBufferGeometry( 400, 300 );
var matFloor = new THREE.MeshPhongMaterial();
var mshFloor = new THREE.Mesh( geoFloor, matFloor );
mshFloor.receiveShadow = true;
mshFloor.position.set( 0, - 0.05, 0 );
mshFloor.ignoreIntersect = true;
scene.add(mshFloor)


// Lights
// var light = new THREE.SpotLight( 0xFF7F00  , 2 );
var light = new THREE.PointLight( 0xffffff );
var ambient = new THREE.AmbientLight( 0x222222 );
// light.position.set( 4, 1, 15 ).normalize();
// light.lookAt(new Vector3(0,0,0))
light.castShadow = true
// light.angle = 0.3;
// light.penumbra = 0.2;
// light.decay = 2;
// light.distance = 50;
// scene.add( light );
scene.add(ambient)

// light.distance = 150;
light.position.setY(120)
scene.add( light );

// buildings
var geometry = new THREE.BoxBufferGeometry( 20, 20, 20 );
var object = new THREE.Mesh( geometry, new THREE.MeshPhongMaterial( { color: Math.random() * 0xffffff } ) );
var object2 = new THREE.Mesh( geometry, new THREE.MeshPhongMaterial( { color: Math.random() * 0xffffff } ) );
object.position.set(0, 10, 0)
object2.position.set(-35, 10, 10)
object.castShadow = true;
object.receiveShadow = true
// scene.add(object, object2)


//shape test
var heartShape = new THREE.Shape();
var x = 0, y = 0;
heartShape.moveTo( x + 5, y + 5 );
heartShape.bezierCurveTo( x + 5, y + 5, x + 4, y, x, y );
heartShape.bezierCurveTo( x - 6, y, x - 6, y + 7,x - 6, y + 7 );
heartShape.bezierCurveTo( x - 6, y + 11, x - 3, y + 15.4, x + 5, y + 19 );
heartShape.bezierCurveTo( x + 12, y + 15.4, x + 16, y + 11, x + 16, y + 7 );
heartShape.bezierCurveTo( x + 16, y + 7, x + 16, y, x + 10, y );
heartShape.bezierCurveTo( x + 7, y, x + 5, y + 5, x + 5, y + 5 );
var extrudeSettings = { amount: 8, bevelEnabled: true, bevelSegments: 2, steps: 2, bevelSize: 1, bevelThickness: 1 };
var geometry = new THREE.ExtrudeGeometry( heartShape, extrudeSettings );
var mesh = new THREE.Mesh( geometry, new THREE.MeshPhongMaterial({ color: Math.random() * 0xffffff }) );
mesh.position.set(-20, 40, 0)
mesh.castShadow = true;
// mesh.rotateZ(180)
scene.add(mesh);

renderer.render(scene, camera)
renderer.setPixelRatio( window.devicePixelRatio );
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap;
renderer.outputEncoding = THREE.sRGBEncoding;
document.addEventListener('mousemove', onDocumentMouseMove, false)


var controls = new TrackballControls( camera, renderer.domElement );
controls.minDistance = 200;
controls.maxDistance = 500;
                

// model
var loader = new GLTFLoader();
// Load a glTF resource
loader.load(
	'assets/tower1.glb',
	function ( gltf ) {
        window.tower = gltf;
		scene.add( window.tower );
		gltf.animations; // Array<THREE.AnimationClip>
		gltf.scene; // THREE.Scene
		gltf.scenes; // Array<THREE.Scene>
		gltf.cameras; // Array<THREE.Camera>
		gltf.asset; // Object
	},
	// called while loading is progressing
	function ( xhr ) {
		console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
	},
	// called when loading has errors
	function ( error ) {
		console.log( 'An error happened' );
	}
);


animate()
drawHexagons()

function drawHexagons() {

    const radius = 4;
    const horizontalHalf = 17.5; // width will be 2x
    const verticalHalf = 30;

    const offsetX = 0;
    const offsetY = 0;

    for (let i = -radius; i <= radius; i++) {
        const r1 = Math.max(-radius, -i - radius);
        const r2 = Math.min(radius, -i + radius);

        for (let j = r1; j <= r2; j++) {
            // let hexagon = new Hexagon({
            //     scene: config.scene,
            //     key: 'hexagon',
            //     x: offsetX + (horizontalHalf * j + (horizontalHalf*2*i)),
            //     y: offsetY + (-j * verticalHalf)
            // });
            const x = offsetX + (horizontalHalf * j + (horizontalHalf*2*i));
            const y =  offsetY + (-j * verticalHalf);

            let r = 20;
            let shape = [];
            let points = new THREE.Shape();
            for(let a = 0; a <= 6; a++) {

                shape[a] = new Vector3(
                    r * Math.sin(a * 60 * Math.PI / 180), 
                    1, // y doesn't matter
                    r * Math.cos(a * 60 * Math.PI / 180) //*0.6
                )
                if (a == 0) {
                    points.moveTo(r * Math.sin(a * 60 * Math.PI / 180), r * Math.cos(a * 60 * Math.PI / 180))
                } else {
                    points.lineTo(r * Math.sin(a * 60 * Math.PI / 180), r * Math.cos(a * 60 * Math.PI / 180))
                }

                var extrudeSettings = {
                    steps: 2,
                    depth: 1,
                    bevelEnabled: false,
                    bevelThickness: 2,
                    bevelSize: 2,
                    bevelOffset: 0,
                    bevelSegments: 2
                };

                // window.game.shape = shape;

                // var points = [];
                // points.push( new THREE.Vector3( -10, 10, 0 ) );
                // points.push( new THREE.Vector3( 0, 10, 0 ) );
                // points.push( new THREE.Vector3( 10, 0, 0 ) );
            
            }

            var wireGeo = new THREE.ShapeGeometry( points );
            // var line = new THREE.Line( wireGeo, material );

            // var geometry = new THREE.BufferGeometry().setFromPoints( shape );
            // var mesh = new THREE.Line( geometry, new THREE.MeshPhongMaterial({ color: Math.random() * 0xffffff })  );
            // mesh.position.set(x, 0, y)

            var geometry = new THREE.ExtrudeGeometry( points, extrudeSettings );
            var mesh = new THREE.Mesh( geometry, new THREE.MeshPhongMaterial( { color: 0x00ff00 } ) ) ;
            // mesh.position.set(new Vector3(2, -50, 0))
            mesh.position.set(x, 0, y)
            window.mesh = mesh;
            mesh.receiveShadow = true;
            mesh.castShadow  = true;
            mesh.rotateX(-1.5708)
            scene.add(mesh)

        }

    }




            
}


function onDocumentMouseMove(event) {
    // the following line would stop any other event handler from firing
    // (such as the mouse's TrackballControls)
    // event.preventDefault();
  
    // update the mouse variable
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
}


function animate() {
    requestAnimationFrame(animate);
    render();
    update();
    controls.update();
}

function render() {
    camera.updateMatrixWorld();
    
    ray = new THREE.Raycaster();
    ray.setFromCamera( mouse, camera );
    let intersects = ray.intersectObjects(scene.children);
  
    // // INTERSECTED = the object in the scene currently closest to the camera 
    // //		and intersected by the Ray projected from the mouse position 	
  
    // // if there is one (or more) intersections
    if (intersects.length > 0) {
      // if the closest object intersected is not the currently stored intersection object
      if (intersects[0].object != INTERSECTED && !intersects[0].object.ignoreIntersect) {

        // restore previous intersection object (if it exists) to its original color
        if (INTERSECTED)
          INTERSECTED.material.color.setHex(INTERSECTED.currentHex);
        // store reference to closest object as current intersection object
        INTERSECTED = intersects[0].object;
        // store color of closest object (for later restoration)
        INTERSECTED.currentHex = INTERSECTED.material.color.getHex();
        // set a new color for closest object
        INTERSECTED.material.color.setHex(0xffff00);
      }
    } else // there are no intersections
    {
      // restore previous intersection object (if it exists) to its original color
      if (INTERSECTED)
        INTERSECTED.material.color.setHex(INTERSECTED.currentHex);
      // remove previous intersection object reference
      //     by setting current intersection object to "nothing"
      INTERSECTED = null;
    }

    mshFloor.setRotationFromAxisAngle(new THREE.Vector3(1,0,0), window.game.radius)

    renderer.render(scene, camera);

}

function update() {
    // console.log(mouse)




    // // find intersections
  
    // // create a Ray with origin at the mouse position
    // //   and direction into the scene (camera direction)
    // var vector = new THREE.Vector3(mouse.x, mouse.y, 1);
    // vector.unproject(camera);
    
  

    
  
    // controls.update();
    // stats.update();
}

// class Init  {

//     init = () => {
//         console.log('test');
//         const loop = new Loop;
//         var config = {
//             type: Phaser.AUTO,
//             width: window.innerWidth,
//             height: window.innerHeight,
//             physics: {
//                 default: 'arcade',
//                 arcade: {
//                     gravity: { y: 200 }
//                 }
//             },
//             scene: {
//                 preload: this.preload,
//                 create: this.create,
//                 update: this.update
//             }
//         };
    
//         window.game = new Phaser.Game(config);

//         window.addEventListener('resize', function (event) {
//             game.scale.resize(window.innerWidth, window.innerHeight);                    
//         }, false);
//         console.log(window.innerWidth + "x" + window.innerHeight);
//     }
// }

// window.game = {};

// const config = {
//     type: Phaser.AUTO,
//     width: window.innerWidth,
//     height: window.innerHeight,
//     parent: 'play',
//     scene: [
//         Preloader
//     ]
// }
// const game = new Phaser.Game(config)

// window.addEventListener('resize', (e) => {
//     game.scale.resize(window.innerWidth, window.innerHeight);
//     // console.log(game.scene);
//     // game.scene.scene.windowUpdate();
//     game.scene.scenes[0].windowUpdate();
// }, false);

// const init = new Init();
// init.init();

